/*
 * @Author: N0ts
 * @Date: 2022-01-11 09:08:32
 * @LastEditTime: 2022-06-08 01:00:17
 * @Description: Vue 配置
 * @FilePath: /vue/vue.config.js
 * @Mail：mail@n0ts.cn
 */

module.exports = {
    // publicPath: "../beta",
    pages: {
        index: {
            // page 的入口
            entry: "src/main.js",
            // title
            title: "IceCream 冰激凌 | 简约至上"
        }
    }
};
