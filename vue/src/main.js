/*
 * @Author: N0ts
 * @Date: 2022-01-10 15:27:28
 * @LastEditTime: 2022-01-12 18:02:52
 * @Description: Main
 * @FilePath: /vue/src/main.js
 * @Mail：mail@n0ts.cn
 */

import { createApp } from "vue";
import "element-plus/dist/index.css";

import App from "./App.vue";
const app = createApp(App);

app.mount("#app");
